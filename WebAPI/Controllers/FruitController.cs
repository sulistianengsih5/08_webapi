﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics.Eventing.Reader;
using System.Threading.Tasks;
using WebAPI.Data;
using WebAPI.Model;

namespace WebAPI.Controllers
{
    [Authorize]
    [Route(("[controller]"))]
    [ApiController]
    public class FruitController : ControllerBase
    {
        private readonly WebAPIContext _dbContext;
        
        public FruitController(WebAPIContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet("GetAllFruits")]
        public async Task<ActionResult<IEnumerable<Fruit>>> GetAllFruits()
        {
            return await _dbContext.Fruit.ToListAsync();
        }

        [HttpGet("GetFruit/{id}")]
        public async Task<ActionResult<Fruit>> GetFruit(string id)
        {
            var fruit = await _dbContext.Fruit.FindAsync(id);

            if (fruit == null)
            {
                return NotFound();
            }

            return fruit;
        }

        [HttpPost("AddFruit")]
        public async Task<ActionResult<Fruit>> AddFruit(Fruit fruit)
        {
            _dbContext.Fruit.Add(fruit);
            await _dbContext.SaveChangesAsync();

            return CreatedAtAction("GetFruit", new {id = fruit.Id}, fruit);
        }

        [HttpPut("UpdateFruit/{id}")]
        public async Task<IActionResult> UpdateFruit(string id, Fruit fruit)
        {
            if (id != fruit.Id)
            {
                return BadRequest();
            }

            _dbContext.Update(fruit);

            _dbContext.Entry(fruit).State = EntityState.Modified;

            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return BadRequest();
            }

            return Ok();
        }

        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult<Fruit>> DeleteFruit (string id)
        {
            var fruit = await _dbContext.Fruit.FindAsync(id);
            if (fruit == null)
            {
                return NotFound();
            }

            _dbContext.Fruit.Remove(fruit);
            await _dbContext.SaveChangesAsync();
            return fruit;
        }
    }
}
