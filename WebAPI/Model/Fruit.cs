﻿namespace WebAPI.Model
{
    public class Fruit
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Taste { get; set; }
    }
}
