﻿using Microsoft.AspNetCore.Identity;
using WebAPIJWT.Models;

namespace WebAPIJWT.Repositories
{
    public class UserRepository
    {
        public static List<User> Users = new()
        {
            new() { Username = "rayhan_admin", EmailAddress = "rayhan.admin@gmail.com", Password = "MyPass_w0rd", GivenName = "Rayhan", Surname = "Tampan",
            Role = "Administrator"},
            new() { Username = "hakeem_admin", EmailAddress = "hakeem.admin@gmail.com", Password = "MyPass_w0rd", GivenName = "Hakeem", Surname = "Berani",
            Role = "Standard"},
        };
    }
}
