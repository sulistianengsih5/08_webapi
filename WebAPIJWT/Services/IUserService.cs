﻿using WebAPIJWT.Models;

namespace WebAPIJWT.Services
{
    public interface IUserService
    {
        public User Get(UserLogin userLogin);
    }
}
